;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "Source Code Pro"))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-dracula)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type `relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-z"))
(global-set-key (kbd "C-/")   'undo-fu-only-undo)
(global-set-key (kbd "C-?") 'undo-fu-only-redo)
(global-visual-line-mode t)
(setq tab-width 4)
(remove-hook! flycheck-mode  #'+syntax-init-popups-h)
(envrc-global-mode)

;; wcheck
(defvar my-finnish-syntax-table
  (copy-syntax-table text-mode-syntax-table))

(modify-syntax-entry ?- "w" my-finnish-syntax-table)

(setq wcheck-language-data
      '(("Finnish"
       (program . "/usr/bin/enchant-2")
       (args "-l" "-d" "fi")
       (syntax . my-finnish-syntax-table)
       (action-program . "/usr/bin/enchant-2")
       (action-args "-a" "-d" "fi")
       (action-parser . wcheck-parser-ispell-suggestions))))

(after! highlight-indentation
(set-face-background 'highlight-indentation-face "#44475a")
(set-face-background 'highlight-indentation-current-column-face "#44475a")
)
(global-display-fill-column-indicator-mode 1)
